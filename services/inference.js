const axios = require("axios");

const URL = "https://inference-runner.hw.ask-ai.co";
async function inferenceAskQuestion(question) {
  try {
    const result = await axios({
      method: "post",
      url: `${URL}/ask`,
      headers: {
        "x-api-key": "7c4e87e6-aef8-467a-b43a-4f80147453bf",
        "Content-Type": "application/json",
      },
      data: {
        question,
      },
    });

    return result.data;
  } catch (e) {
    throw e;
  }
}

module.exports = {
  inferenceAskQuestion,
};
